from scipy import ndimage as ndi
import matplotlib.pyplot as plt
from skimage.color import rgb2gray, gray2rgb, label2rgb
from skimage.morphology import disk
from skimage.segmentation import watershed
from skimage import data
from skimage.filters import rank
from skimage.util import img_as_ubyte
from skimage.filters import threshold_otsu
from skimage.exposure import histogram
import numpy as np
from skimage.io import imread
from scipy import ndimage as ndi
from skimage.morphology import  remove_small_holes
from deap import base
from deap import creator
from deap import tools
import random
import glob
import os
import seaborn as sns
from time import sleep
def w0_back(t_level, P_i):
    index = t_level - 1
    return np.sum(P_i[:index])
def w1_front(t_level, P_i):
    return np.sum(P_i[t_level:])
def mean_back(t_level, P_i):
    grey_levels = np.arange(0, 256)
    count_p_P = grey_levels[:t_level - 1] *  P_i[:t_level - 1]
    sum_p_P = np.sum(count_p_P)
    w0_v = w0_back(t_level,P_i)
    res = sum_p_P / w0_v
    return res
def mean_front(t_level, P_i):
    grey_levels = np.arange(0, 256)
    count_p_P = grey_levels[t_level - 1:] *  P_i[t_level - 1:]
    sum_p_P = np.sum(count_p_P)
    w1_v = w1_front(t_level, P_i)
    res = sum_p_P / w1_v
    return res
def m_avg_grey_v(P_i):
    grey_levels = np.arange(0, 256)
    val = np.sum(P_i *  grey_levels)
    return val
def variance_back(t_level):
    m0_value = mean_back(t_level)
    w0_value = w0_back(t_level)
    sum = 0
    for t in range(0, t_level - 1):
        sum += (t - m0_value) ** 2 / w0_value
    return sum
def variance_front(t_level,P_i):
    m1_value = mean_front(t_level, P_i)
    w1_value = w1_front(t_level,P_i)
    sum = 0
    for t in range(0, t_level - 1):
        sum += (t - m1_value) ** 2 / w1_value
    return sum
def internal_class_variance(t_level,P_i):
    w0_value = w0_back(t_level, P_i)
    w1_value = w1_front(t_level, P_i)
    variance_back_value = variance_back(t_level,P_i)
    variance_front_value = variance_front(t_level, P_i)
    res = w0_value * variance_back_value + w1_value * variance_front_value
    return res
def beetwen_class_variance(t_level, P_i):
    w0_value = w0_back(t_level, P_i)
    m0_value = mean_back(t_level,P_i)
    w1_value = w1_front(t_level,P_i)
    m1_value = mean_front(t_level,P_i)
    res = w0_value * w1_value * (m1_value - m0_value) ** 2
    return res
def population_variance(t_level):
    return internal_class_variance(t_level) + beetwen_class_variance(t_level)
ONE_MAX_LENGTH = 8  # length of bit string to be optimized
# Genetic Algorithm constants:
POPULATION_SIZE = 50
P_CROSSOVER = 0.9  # probability for crossover
P_MUTATION = 0.2  # probability for mutating an individual
MAX_GENERATIONS = 50
# set the random seed:
RANDOM_SEED = 42
random.seed(RANDOM_SEED)
toolbox = base.Toolbox()
# create an operator that randomly returns 0 or 1:
toolbox.register("zeroOrOne", random.randint, 0, 1)
# define a single objective, maximizing fitness strategy:
creator.create("FitnessMax", base.Fitness, weights=(1.0,))
# create the Individual class based on list:
creator.create("Individual", list, fitness=creator.FitnessMax)
# creator.create("Individual", array.array, typecode='b', fitness=creator.FitnessMax)
# create the individual operator to fill up an Individual instance:
toolbox.register("individualCreator", tools.initRepeat, creator.Individual, toolbox.zeroOrOne, ONE_MAX_LENGTH)
# create the population operator to generate a list of individuals:
toolbox.register("populationCreator", tools.initRepeat, list, toolbox.individualCreator)
# fitness calculation:
# compute the number of '1's in the individual
def imageBeetwenClassVariance(individual, P_i):
    input_array = np.array(individual)
    t_level = input_array.dot(2 ** np.arange(input_array.size)[::-1])
    if t_level==0 or t_level==256:
        return 0,
    fval = beetwen_class_variance(t_level, P_i)
    if   fval=='nan':
        print("stop")
    return fval,
toolbox.register("evaluate", imageBeetwenClassVariance)
# genetic operators:
# Tournament selection with tournament size of 3:
toolbox.register("select", tools.selTournament, tournsize=3)
# Single-point crossover:
toolbox.register("mate", tools.cxOnePoint)
# Flip-bit mutation:
# indpb: Independent probability for each attribute to be flipped
toolbox.register("mutate", tools.mutFlipBit, indpb=1.0 / ONE_MAX_LENGTH)
# Genetic Algorithm flow:
def main_ga(P_i):
    # create initial population (generation 0):
    population =  toolbox.populationCreator(n=POPULATION_SIZE)
    generationCounter = 0
    # calculate fitness tuple for each individual in the population:
    fitnessValues=[]
    for individ in population:
        res = toolbox.evaluate(individ, P_i)
        fitnessValues.append(res)
    #fitnessValues = list(map( toolbox.evaluate, population))
    for individual, fitnessValue in zip(population, fitnessValues):
        individual.fitness.values = fitnessValue
    # extract fitness values from all individuals in population:
    fitnessValues = [individual.fitness.values[0] for individual in population]
    # initialize statistics accumulators:
    maxFitnessValues = []
    meanFitnessValues = []
    # main evolutionary loop:
    # stop if max fitness value reached the known max value
    # OR if number of generations exceeded the preset value:
    cur_max = max(fitnessValues)
    while generationCounter <  MAX_GENERATIONS:
        # update counter:
        generationCounter = generationCounter + 1
        # apply the selection operator, to select the next generation's individuals:
        offspring =  toolbox.select(population, len(population))
        # clone the selected individuals:
        offspring = list(map( toolbox.clone, offspring))
        # apply the crossover operator to pairs of offspring:
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < P_CROSSOVER:
                toolbox.mate(child1, child2)
                del child1.fitness.values
                del child2.fitness.values
        for mutant in offspring:
            if random.random() <  P_MUTATION:
                toolbox.mutate(mutant)
                del mutant.fitness.values
        # calculate fitness for the individuals with no previous calculated fitness value:
        freshIndividuals = [ind for ind in offspring if not ind.fitness.valid]
        freshFitnessValues=[]
        for individ in freshIndividuals:
            res = toolbox.evaluate(individ, P_i)
            freshFitnessValues.append(res)
        #freshFitnessValues = list(map(toolbox.evaluate, freshIndividuals))
        for individual, fitnessValue in zip(freshIndividuals, freshFitnessValues):
            individual.fitness.values = fitnessValue
        # replace the current population with the offspring:
        population[:] = offspring
        # collect fitnessValues into a list, update statistics and print:
        fitnessValues = [ind.fitness.values[0] for ind in population]
        maxFitness = max(fitnessValues)
        meanFitness = sum(fitnessValues) / len(population)
        maxFitnessValues.append(maxFitness)
        meanFitnessValues.append(meanFitness)
        if meanFitness==None:
            print("stop")
        print("- Generation {}: Max Fitness = {}, Avg Fitness = {}".format(generationCounter, maxFitness,
                                                                           meanFitness))
    # find and print best individual:
    best_index = fitnessValues.index(max(fitnessValues))
    print("Best Individual = ", *population[best_index], "\n")
    # Genetic Algorithm is done - plot statistics:
    sns.set_style("whitegrid")
    plt.plot(maxFitnessValues, color='red')
    plt.plot(meanFitnessValues, color='green')
    plt.xlabel('Кол-во итераций')
    plt.ylabel('Max / Average Fitness')
    plt.title('Max and Average Fitness over Generations')
    plt.show()
    solution_array =  np.array(population[best_index])
    t_v = solution_array.dot(2 ** np.arange(solution_array.size)[::-1])
    return t_v
class Image_Segmentator:
    def __init__(self, path_to_images="images/ima"):
        self.root_director = path_to_images
        # problem constants:
    def perform_segmentation(self, image_path):
        img_color = imread(image_path)
        img = rgb2gray(img_color)
        neg_img = 1 - img
        th_level = threshold_otsu(neg_img)
        denoised = rank.median(neg_img, disk(2))
        denoised = denoised / 256
        #self.grey_levels = np.arange(0, 256)
        hist, hist_centers = histogram(denoised, nbins=256)
        N = np.sum(hist)
        P_i = hist / N
        custom_level = main_ga(P_i)
        th_int_level = int(th_level * 256)
        res1 =  beetwen_class_variance(th_int_level,P_i)
        res2 =  beetwen_class_variance(custom_level,P_i)
        plt.hist(img.ravel(), bins=256)
        fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(10, 10), sharex=True, sharey=True)
        ax = axes.ravel()
        ax[0].imshow(img_color, cmap=plt.cm.gray)
        ax[1].imshow(neg_img, cmap=plt.cm.gray)
        #
        # #Otsu method
        seg_img = neg_img > th_level
        seg_img_2 = neg_img > custom_level / 256
        ax[2].imshow(seg_img, cmap=plt.cm.gray)
        ax[2].set_xlabel("Значение целовой функции - "+str(int(res1)))
        color = ndi.binary_fill_holes(seg_img)
        labeled_coins, _ = ndi.label(color)
        image_label_overlay = label2rgb(labeled_coins, image=img)
        ax[3].imshow(image_label_overlay)
        ax[3].axis('off')
        #Genetic method

        ax[4].imshow(seg_img_2, cmap=plt.cm.gray)
        color_2 = ndi.binary_fill_holes(seg_img_2)
        labeled_2, _ = ndi.label(color_2)
        image_label_overlay_2 = label2rgb(labeled_2, image=img)
        ax[4].set_xlabel("Значение целовой функции - "+str(int(res2)))
        ax[5].imshow(image_label_overlay_2)
        ax[5].axis('off')
        plt.show()
        sleep(2)
    def start_test(self):
        path = self.root_director
        files = glob.glob(path+"/*.jpg")
        files.sort(key=os.path.getmtime)
        for f in files:
           self.perform_segmentation(f)
        print("Finish")
segmentator = Image_Segmentator("images")
segmentator.start_test()







